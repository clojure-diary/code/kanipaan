(ns kanipaan.core
  (:gen-class :main true)
  (:require [kanipaan.calc :as calc]))

(defn prompt [message]
  (print message)
  (flush)
  (read-line))

(defn -main []
   (let [input (prompt "calc >  ")]
     (when-not (clojure.string/blank? input)
       (println (calc/parse input))
       (recur))))

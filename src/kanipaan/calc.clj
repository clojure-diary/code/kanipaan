(ns kanipaan.calc)

(defn tokenize [string]
  (re-find #"(\d+\.?\d*)\s*([\+\-\*\/])\s*(\d+\.?\d*)" string))

(defn needs-parsing? [string]
  (not (nil? (tokenize string))))

(defn calculate [numbers-and-operator]
  (let [operator (second numbers-and-operator)
        first-number (read-string (first numbers-and-operator))
        second-number (read-string (nth numbers-and-operator 2))]
    (case operator
      "+" (+ first-number second-number)
      "-" (- first-number second-number)
      "*" (* first-number second-number)
      "/" (/ first-number second-number))))

(defn simplify [string]
  (let [tokens (tokenize string)
        numbers-and-operator (rest tokens)
        calculated-value (calculate numbers-and-operator)]
    (clojure.string/replace-first string (first tokens) (str calculated-value))))

(defn parse [string]
  (if (not (needs-parsing? string))
    string
    (recur (simplify string))))